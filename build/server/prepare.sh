#!/bin/bash
su - l -c 'cp /app/config/env /app/laravel/.env'
su - l -c "cd /app/laravel && composer install"
su - l -c "cd /app/laravel && \
            php artisan key:generate && \
            php artisan migrate:fresh --seed"