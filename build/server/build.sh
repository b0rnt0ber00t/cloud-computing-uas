#!/bin/bash
echo "================================"

# package
echo "[+] update package"
apt-get update
apt-get full-upgrade -y

# php extention
echo "[+] run docker-php-ext-install bcmath"
docker-php-ext-install bcmath > /dev/null
echo "[+] run docker-php-ext-install mysqli"
docker-php-ext-install mysqli > /dev/null
echo "[+] run docker-php-ext-install mysqlnd"
docker-php-ext-install mysqlnd > /dev/null
echo "[+] run docker-php-ext-install pdo_mysql"
docker-php-ext-install pdo_mysql > /dev/null
echo "[+] run docker-php-ext-install gd"
apt-get install libpng-dev -y > /dev/null
docker-php-ext-install gd > /dev/null
echo "[+] run docker-php-ext-install zip"
apt-get install libzip-dev zip -y > /dev/null
docker-php-ext-install zip > /dev/null

# composer
echo "[+] install composer"
curl -fsSL https://getcomposer.org/installer | \
  php -- --install-dir=/usr/local/bin --filename=composer

# nodejs
echo "[+] install nodejs"
curl -fsSL https://deb.nodesource.com/setup_lts.x | \
  bash -
apt-get install -y nodejs gcc g++ make

# yarn
echo "[+] install yarn"
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | \
  gpg --dearmor | \
  tee /usr/share/keyrings/yarnkey.gpg > /dev/null
echo "deb [signed-by=/usr/share/keyrings/yarnkey.gpg] https://dl.yarnpkg.com/debian stable main" | \
  tee /etc/apt/sources.list.d/yarn.list
apt-get update
apt-get install -y yarn

# git
echo "[+] install git"
apt install -y git

# shell
echo "[+] install zsh"
apt install -y zsh
echo "[+] create user"
useradd -m l -s $(which zsh)

# omz
echo "[+] install omz"
su - l -c "curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh | sh -"