echo "[+] run docker compose build"
docker compose build --no-cache

echo "[+] run docker compose up $1 $2"
docker compose up $1 $2

echo "[+] run docker compose down"
docker compose down --rmi all --remove-orphans

echo "[+] run docker volume prune -f"
docker volume prune -f